using Microsoft.EntityFrameworkCore;

namespace myfilm.Models
{
    public class myfilmContext : DbContext
    {
        public myfilmContext (DbContextOptions<myfilmContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movie { get; set; }
    }
}