using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace myfilm.Controllers
{
    public class HelloWorldController : Controller
    {
        // 
        // GET: /HelloWorld/
        // GET: /HelloWorld/Welcome/ 
        // Requires using System.Text.Encodings.Web;
        public string Welcomeold(string N, int numTimes = 1)
        {
            return HtmlEncoder.Default.Encode($"Hello {N}, NumTimes is: {numTimes}");
        }

        //public string Index()
        //{
         //   return "This is my default action.. hallo ari.";
        //}

        public IActionResult Index()
        {
            ViewData["NumTimes"] = 10;
            ViewData["Message"] = "hallo";
            return View();
        }

         public IActionResult Welcome(string name, int numTimes = 1)
        {
            ViewData["Message"] = "Hello " + name;
            ViewData["NumTimes"] = numTimes;

            return View();
        }
    }
}
